﻿#light

module Curve
[<Sealed>]
type curve =
    static member ( + ) : curve * curve -> curve
    static member ( * ) : float * curve -> curve
    static member ( |^) : curve * float -> curve
    static member ( |^) : curve * int -> curve
    static member (-->) : curve * (float * float) -> curve
    static member (><) : curve * float -> curve ;;
val point : float * float -> curve ;;
val verticRefl : curve -> float -> curve ;;
val boundingBox : curve -> (float * float) * (float * float) ;;
val width : curve -> float ;;
val height : curve -> float ;;
val toList : curve -> (float * float) list ;;
