﻿#light

//per referenziare la dll nell'interprete
//#r @"c:\Users\matteo-win\Documents\Visual Studio 2010\Projects\VisualFSharp\Curve\bin\Debug\Curve.dll"

module Curve

type curve = C of (float*float) * ((float*float) list) ;;

let map f (C(p0,ps)) = C(f p0,List.map f ps) ;;
let mapP g (C(p0,ps)) = C(p0,List.map (g p0) ps) ;;

type curve with
    static member(+) (c1:curve, c2:curve) =
        match (c1,c2) with
        | (C(p1,ps1),C(p2,ps2)) -> C(p1,ps1@(p2::ps2))
    static member (*) (a: float, c: curve) =
        let multA (x0,y0) (x,y) = (x0 + a * (x - x0), y0 + a * (y - y0))
        mapP multA c
    static member (|^) (c:curve, ang: float) =
        let piFact = System.Math.PI / 180.0
        let cs = cos (piFact * ang)
        let sn = sin (piFact * ang)
        let rot (x0,y0) (x,y) =
            let (dx,dy) = (x - x0,y - y0)
            (x0 + cs * dx - sn * dy, y0 + sn * dx + cs * dy)
        mapP rot c
    static member (|^) (c:curve, ang: int) = c |^ (float ang)
    static member (-->) (c: curve, (x1,y1): float*float) =
        match c with
        | C((x0,y0),_) -> map (fun (x,y) -> (x-x0+x1, y-y0+y1)) c
    static member (><) (c:curve, a: float) =
        map (fun (x,y) -> (2.0 * a - x, y)) c ;;

let point (p: float*float) = C (p,[]) ;;
let verticRefl (c:curve) (b:float) = map (fun (x,y) -> (x, 2.0*b - y)) c
let boundingBox (C((px,py),ps)) =
    let minmax ((minX,minY),(maxX,maxY)) ((x,y):float*float) =
        ((min minX x, min minY y),(max maxX x, max maxY y))
    List.fold minmax ((px,py),(px,py)) ps
let width (c:curve) = 
    let ((minX,_),(maxX,_)) = boundingBox c
    maxX - minX ;;
let height (c:curve) = 
    let ((_,minY),(_,maxY)) = boundingBox c
    maxY - minY ;;
let toList (C(p,ps)) = p :: ps ;;